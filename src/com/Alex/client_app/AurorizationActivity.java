package com.Alex.client_app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class AurorizationActivity extends Activity {
	EditText login_txt,password_txt;
	ProgressDialog prgDialog;
	public static String FILENAME="login.psw";
	String access_token="";
	boolean autor=false;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aurorization);
        login_txt=(EditText)findViewById(R.id.editLoginreg);
        password_txt=(EditText)findViewById(R.id.editPasswordReg);
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please wait...");
        prgDialog.setCancelable(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.aurorization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_exit) {
        	android.os.Process.killProcess(android.os.Process.myPid());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void onEnterClick(View view){
		String login=login_txt.getText().toString();
		String password=password_txt.getText().toString();
		String client_key="";
		String client_secret="";
		String grant_type="password";
		String[] data;
		
		if(login.length()!=0&&password.length()!=0){
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(
				          openFileInput(FILENAME)));
				String str = "";
			      while ((str = br.readLine()) != null) {
			    	  data=str.split(";");
			    	  if(data[0].equals(login)){
			    		  client_key=data[1];
			    		  client_secret=data[2];
			    		  autor=true;
			    		  break;
			    	  }
			      }
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
			      e.printStackTrace();
		    }
			if(autor==true){
				autor=false;
				autorization(login,password,client_key,client_secret,grant_type);
			}else{
				Toast.makeText(getApplicationContext(), "������������ � ����� ������� �� ���������������", Toast.LENGTH_LONG).show();
			}
		}else{
			Toast.makeText(getApplicationContext(), "���������� ������ ����� � ������", Toast.LENGTH_LONG).show();
		}
    }
    
    public void autorization(String login,String password,String client_key,String client_secret,String grant_type){
    	prgDialog.show();
    	AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		
		params.put("username",login);
		params.put("password", password);
		params.put("client_id",client_key);
		params.put("client_secret", client_secret);
		params.put("grant_type", grant_type);
		client.post("http://smktesting.herokuapp.com/api/oauth2/access_token/",params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
					JSONObject o=new JSONObject(response);
					access_token=o.getString("refresh_token");
					autor=true;
					Intent intent=new Intent(AurorizationActivity.this,ProductsActivity.class);
					intent.putExtra("access_token",access_token);
					intent.putExtra("autor", autor);
					startActivity(intent);
				} catch (JSONException e) {
					e.printStackTrace();
				}
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // Hide Progress Dialog 
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } 
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } 
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    
    public void onRegClick(View view){
    	Intent regIntent=new Intent(AurorizationActivity.this,RegistrationActivity.class);
    	startActivity(regIntent);
    }
    
    public void onWithAutoClick(View view){
    	autor=false;
    	Intent prodIntent=new Intent(AurorizationActivity.this,ProductsActivity.class);
    	prodIntent.putExtra("autor", autor);
    	startActivity(prodIntent);
    }
}
