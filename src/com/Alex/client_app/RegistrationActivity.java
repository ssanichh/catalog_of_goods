package com.Alex.client_app;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends Activity {
	EditText login_txt,password_txt;
	ProgressDialog prgDialog;
	public static String FILENAME="login.psw";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		login_txt=(EditText)findViewById(R.id.editLoginreg);
		password_txt=(EditText)findViewById(R.id.editPasswordReg);
		prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please wait...");
        prgDialog.setCancelable(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_exit) {
			android.os.Process.killProcess(android.os.Process.myPid());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onRegClick(View view){
		final String login=login_txt.getText().toString();
		String password=password_txt.getText().toString();
		prgDialog.show();
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		if(login.length()!=0&&password.length()!=0){
		
			params.put("username",login);
			params.put("password", password);
			
		client.post("http://smktesting.herokuapp.com/api/register/",params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
            	boolean suc=false;
                // Hide Progress Dialog
                prgDialog.hide();
                try {
					JSONObject o=new JSONObject(response);
					suc=o.getBoolean("success");
					if(suc==true){
	                	try {
	                		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
	                				openFileOutput(FILENAME, MODE_APPEND)));
	                		bw.write(login+";"+o.getString("client_key")+";"+o.getString("client_secret")+"\n");
	                		bw.close();
	                		Toast.makeText(getApplicationContext(),"����������� �������", Toast.LENGTH_LONG).show();
	                	} catch (FileNotFoundException e) {
	                		// TODO Auto-generated catch block
	                		e.printStackTrace();
	                	}catch (IOException e) {
	                	      e.printStackTrace();
	                    }
	                }else{
	                	Toast.makeText(getApplicationContext(), "Error: " + o.getString("message"), Toast.LENGTH_LONG).show();
	                }
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
                	
                /*AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
            	builder.setTitle("��������������� ������")
            			.setMessage("��� ���� ������ ��� �����:\n"+response)
            			.setCancelable(false)
            			.setPositiveButton("OK", new OnClickListener(){
            				public void onClick(DialogInterface dialog, int id) {
    							dialog.cancel();
    						}
            			});
            	AlertDialog alert = builder.create();
            	alert.show();*/
                
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // Hide Progress Dialog 
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } 
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } 
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
		}
	}
	
	public void onAutoClick(View view){
		Intent autoIntent=new Intent(RegistrationActivity.this,AurorizationActivity.class);
		startActivity(autoIntent);
	}
}
