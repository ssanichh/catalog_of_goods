package com.Alex.client_app;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class CommentActivity extends Activity {
	RatingBar ratingBar;
	int rate=0;
	int id_prod=0;
	String access_token="";
	String text="";
	TextView comm;
	ProgressDialog prgDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comment);
		
		prgDialog = new ProgressDialog(this);
	    prgDialog.setMessage("Please wait...");
	    prgDialog.setCancelable(false);
	    
		comm=(TextView)findViewById(R.id.editText1);
		id_prod=getIntent().getIntExtra("id", 0);
		access_token=getIntent().getStringExtra("access_token");
		ratingBar=(RatingBar)findViewById(R.id.ratingBar1);
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
	        public void onRatingChanged(RatingBar ratingBar, float rating,
	         boolean fromUser) {
	        	rate=(int)rating;
	        	}
	       });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.comment, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_exit) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onClick(View view){
		text=comm.getText().toString();
		if(!text.equals("")){
			comment();
		}else{
			Toast.makeText(getApplicationContext(), "����������, ������� ��� �����", Toast.LENGTH_LONG).show();
		}
	}
	
	public void comment(){
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("rate", rate);
		params.put("text", text);
		client.addHeader("Authorization", "ssanichh2 "+access_token);
    	client.post("http://smktesting.herokuapp.com/api/reviews/"+id_prod, params, new AsyncHttpResponseHandler() {
		
            @Override
            public void onSuccess(String response) {
                prgDialog.hide();
                Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_LONG).show();
            }
            
            @Override
            public void onFailure(int statusCode, Throwable error, String content) { 
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } 
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } 
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
	}
}
