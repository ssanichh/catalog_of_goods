package com.Alex.client_app;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ProdInfoActivity extends Activity {
	TextView nameProd;
	TextView textProd;
	ImageView imageProd;
	ListView lv;
	ArrayList<Product> products;
	String title,text,image;
	int id_prod;
	ProgressDialog prgDialog;
	String access_token="";
	boolean autor=false;
	private ArrayList<HashMap<String, Object>> list;
	private static final String USER = "user"; 
	private static final String DESCRIPTION = "description"; 
	private static final String RATE = "rate";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prod_info);
		lv=(ListView)findViewById(R.id.listView1);
		nameProd=(TextView)findViewById(R.id.prodName);
		textProd=(TextView)findViewById(R.id.textView2);
		imageProd=(ImageView)findViewById(R.id.imageView1);
		prgDialog = new ProgressDialog(this);
	    prgDialog.setMessage("Please wait...");
	    prgDialog.setCancelable(false);
	    prgDialog.show();
		id_prod=getIntent().getIntExtra("id", 0);
		title=getIntent().getStringExtra("title");
		text=getIntent().getStringExtra("text");
		image=getIntent().getStringExtra("image");
		autor=getIntent().getBooleanExtra("autor", false);
		if(autor==true)access_token=getIntent().getStringExtra("access_token");
		nameProd.setText("�������� - "+title);
		textProd.setText("�������� - "+text);
		
		String url="http://smktesting.herokuapp.com/static/"+image;
		
		try {
			new DownloadImageTask(imageProd)
	        .execute(url);
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "������ �������� �����������.", Toast.LENGTH_LONG).show();
		}
		
		comments();
	}

	private Drawable grabImageFromUrl(String url) throws Exception {
		return Drawable.createFromStream(
				(InputStream) new URL(url).getContent(), "src");
	}
	
	private void comments(){
		list = new ArrayList<HashMap<String, Object>>();
		AsyncHttpClient client = new AsyncHttpClient();
    	client.get("http://smktesting.herokuapp.com/api/reviews/"+id_prod, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String response) {
                prgDialog.hide();
                HashMap<String, Object> hm;
                try {
                	JSONArray obj = new JSONArray(response);
                	for(int i=0;i<obj.length();i++){
                		JSONObject o=obj.getJSONObject(i);
                		hm=new HashMap<String, Object>();
                		JSONObject ou=o.getJSONObject("created_by");
                		hm.put(USER, ou.getString("username"));
                		hm.put(RATE, "������: "+o.getString("rate"));
                		hm.put(DESCRIPTION,"�����: "+ o.getString("text"));
                		list.add(hm);
                	}
                	initList();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            
            @Override
            public void onFailure(int statusCode, Throwable error, String content) { 
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } 
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } 
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
	}
	
	private void initList(){
		SimpleAdapter adapter = new SimpleAdapter(this, list,
				R.layout.list_rate_item, new String[] { USER, DESCRIPTION, RATE},
				new int[] { R.id.userName, R.id.textDescr, R.id.textRate });
		lv.setAdapter(adapter);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.prod_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_exit) {
			android.os.Process.killProcess(android.os.Process.myPid());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onRateClick(View view){
		if(autor==true){
			Intent intent=new Intent(ProdInfoActivity.this,CommentActivity.class);
			intent.putExtra("access_token", access_token);
			intent.putExtra("id", id_prod);
			startActivity(intent);
		}else{
			Toast.makeText(getApplicationContext(), "��������� ����������� ����� ������ ���������������� ������������", Toast.LENGTH_LONG).show();
		}
	}
}
