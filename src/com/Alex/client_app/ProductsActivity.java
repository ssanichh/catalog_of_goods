package com.Alex.client_app;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.view.View;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ProductsActivity extends Activity {
	private static final String NAME = "name";
	ProgressDialog prgDialog;
	private ArrayList<HashMap<String, Object>> list;
	ArrayList<Product> products = new ArrayList<Product>();
	ListView lv;
	String access_token="";
	boolean autor=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_products);
		prgDialog = new ProgressDialog(this);
	    prgDialog.setMessage("Please wait...");
	    prgDialog.setCancelable(false);
	    lv=(ListView)findViewById(R.id.listView1);
	    list = new ArrayList<HashMap<String, Object>>();
	    autor=getIntent().getBooleanExtra("autor", false);
	    if(autor==true)access_token=getIntent().getStringExtra("access_token");
	    
	    prgDialog.show();
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get("http://smktesting.herokuapp.com/api/products/", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String response) {
                prgDialog.hide();
                try {
                	JSONArray obj = new JSONArray(response);
                	for(int i=0;i<obj.length();i++){
                		JSONObject o=obj.getJSONObject(i);
                		int id=o.getInt("id");
                		String img=o.getString("img");
                		String title=o.getString("title");
                		String text=o.getString("text");
                		products.add(new Product(id,title,text,img));
                	}
                	initList();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            
            @Override
            public void onFailure(int statusCode, Throwable error, String content) { 
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } 
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } 
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
	    
	}
	
	public void initList(){
		HashMap<String, Object> hm;
    	for(int i=0;i<products.size();i++){
    		hm=new HashMap<String, Object>();
    		hm.put(NAME, products.get(i).getTitle());
    		list.add(hm);
    	}
    	SimpleAdapter adapter = new SimpleAdapter(this, list,
				R.layout.list_item, new String[] { NAME},
				new int[] { R.id.prodName});
    	lv.setAdapter(adapter);
    	
    	lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		@Override
    		public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
    				long id) {
    			/*Toast.makeText(getApplicationContext(), ((TextView) itemClicked).getText()+". position "+position+" id "+id,
    			        Toast.LENGTH_LONG).show();*/
    			Intent intent=new Intent(ProductsActivity.this,ProdInfoActivity.class);
    			intent.putExtra("id", products.get(position).getId());
    			intent.putExtra("title", products.get(position).getTitle());
    			intent.putExtra("text", products.get(position).getText());
    			intent.putExtra("image", products.get(position).getImg());
    			intent.putExtra("access_token", access_token);
    			intent.putExtra("autor", autor);
    			startActivity(intent);
    		}
    	});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.products, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_exit) {
			android.os.Process.killProcess(android.os.Process.myPid());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
