package com.Alex.client_app;

public class Product {
	private int id;
	private String title;
	private String text;
	private String img;
	
	Product(){
		id=0;
		title="";
		text="";
		img="";
	}
	
	Product(int id,String title,String text,String img){
		this.id=id;
		this.title=title;
		this.text=text;
		this.img=img;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public int getId(){
		return id;
	}
	
	public void setTitle(String title){
		this.title=title;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setText(String text){
		this.text=text;
	}
	
	public String getText(){
		return text;
	}
	
	public void setImg(String img){
		this.img=img;
	}
	
	public String getImg(){
		return img;
	}
}
